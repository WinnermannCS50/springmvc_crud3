package org.example.Lesson22_2.models;

public class Person {
    private int id;
    private String name;

    /**
     * пустой конструктор
     */
    public Person() {
    }

    /**
     * Конструктор с параметрами
     * @param id
     * @param name
     */
    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
