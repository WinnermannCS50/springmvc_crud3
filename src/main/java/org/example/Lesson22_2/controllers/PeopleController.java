package org.example.Lesson22_2.controllers;

import org.example.Lesson22_2.dao.PersonDAO;
import org.example.Lesson22_2.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @Controller - помечает класс как контроллер
 * @RequestMapping("/people") - общее начало HTTP-адреса для всех методов
 */
@Controller
@RequestMapping("/people")
public class PeopleController {

    //Внедряем Бин PersonDAO в PeopleController с помощью Spring
    private final PersonDAO personDAO;

    /**
     * @Autowired - внедряет подходящий Бин в Поле
     * @Autowired - над конструктором можно не писать, все равно внедрится
     *
     * @param personDAO - внедряемый объект для работы с БД(private List<Person> people)
     */
    @Autowired
    public PeopleController(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    /**
     * метод index(Model model) - возвращает список из людей
     * @GetMapping() - в скобках пусто, потому что HTTP остается как и у класса "/people"
     * адрес GET-запроса "/people" для метода index(Model model)
     * @param model
     * @return
     */
    @GetMapping()
    public String index(Model model){
        /**
         * В Model model будет передаваться объект, содержащий в себе список из людей на представление,
         * чтобы Thymeleaf затем отобразил полученный список из людей
         * Получим всех людей из DAO и передадим на отображение в представление
         * Затем с помощью Thymeleaf отобразим полученных людей в представление
         *
         * Вобщем получаем людей, ложим в модель, чтобы передать их в представление
         * под ключем "people" - будет лежать список из людей (people = new ArrayList<>()) из объектов класса Person
         */
        model.addAttribute("people", personDAO.index());

        /**
         * return "people/index" - возвращает шаблон(страницу), которая будет отображать список из людей
         */
        return "people/index";
    }

    /**
     * метод show(@PathVariable("id") int id, Model model) - возвращает одного человека по его id
     * @GetMapping("/{id}") - HTTP-адрес будет /people/id
     * {id}-сюда можно будет поместить любое число после запуска приложения
     * @PathVariable("id") - поместит число из URL из {id} в аргументы метода show()
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/{id}")
    public String show(@PathVariable("id") int id, Model model){
        /**
         * Получим одного человека по id из DAO и передадим на отображение в представление
         * Получаем человека по id, ложим в model, чтобы передать его в представление
         */
        model.addAttribute("person",personDAO.show(id));

        /**
         * Возвращает название того шаблона где будет показыватья этот человек
         * Шаблон называется show и лежит в папке people
         */
        return "people/show";
    }

    /**
     *
     * @param
     * @return
     */
    @GetMapping("/new")
    public String newPerson(@ModelAttribute("person") Person person){

        /**
         * Файл new лежит в папке people
         */
        return "people/new";
    }

    /**
     * redirect: - позволяет направить пользователя с одной страници на другую
     *
     * redirect: -может перенаправлять на разные сервера
     * forward: -перенаправляет внутри одного сервера
     *
     * redirect: -происходит на Клиенте (браузер совершает новый запрос): URL в браузере меняется
     * forward: -происходит на Серевере(Клиент не знает про это): URL в браузере не меняется
     *
     * redirect: медленнее, чем forward: (т.к. необходимо совершить больше операций)
     * @param person
     * @return
     */

    @PostMapping()
    public String create(@ModelAttribute("person") Person person){
        personDAO.save(person);

        return "redirect:/people";
    }
}
