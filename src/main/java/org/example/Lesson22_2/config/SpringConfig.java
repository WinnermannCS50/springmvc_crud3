package org.example.Lesson22_2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;

/**
 * @Configuration - помечает класс SpringConfig как конфигурационный
 *
 * class SpringConfig заменяет файл src/main/webapp/WEB-INF/applicationContextMVC.xml
 *
 * @ComponentScan("org.example.Lesson21") - тоже что <context:component-scan base-package="org.example"/>
 * в файле applicationContextMVC.xml
 *
 * @ComponentScan - Создёт Бины классов помеченных аннотацией @Component
 * @ComponentScan("org.example.Lesson21") - указывает, где лежит контроллер class HelloController
 *
 * @EnableWebMvc -дает возможность приложению поддерживать Web-функции
 * @EnableWebMvc - тоже что <mvc:annotation-driven/> в файле applicationContextMVC.xml
 */
@Configuration
@ComponentScan("org.example.Lesson22_2")
@EnableWebMvc

/**
 * Реализуем interface WebMvcConfigurer, чтобы можно было использовать
 * шаблонизатор Thymeleaf в методе configureViewResolvers()
 */
public class SpringConfig implements WebMvcConfigurer {
    private final ApplicationContext applicationContext;

    /**
     * @param applicationContext - внедряем при помощи аннотации спринга @Autowired
     */
    @Autowired
    public SpringConfig(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Bean
    public SpringResourceTemplateResolver templateResolver() {
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setApplicationContext(applicationContext);
        templateResolver.setPrefix("/WEB-INF/views/");
        templateResolver.setSuffix(".html");
        return templateResolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver());
        templateEngine.setEnableSpringELCompiler(true);
        return templateEngine;
    }

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setTemplateEngine(templateEngine());
        registry.viewResolver(resolver);
    }
}
